set -e

WORK_DIR=${PWD}

echo "[INFO] - check-build-script.sh --> running from: " $WORK_DIR

MODE=""
if [[ $# -eq 1 ]] ; then
    MODE=$1
    echo "[INFO] building with mode = $MODE"
fi


# run build script if present
if [[ -e build.sh ]]
then
	echo "[INFO] build script found. running build.sh $MODE "
	./build.sh $MODE
else
	echo "[INFO] No build script found. Moving on. "
fi
