#!/bin/bash
################################################################################
# Copyright 2022 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# Deploys a deb (or ipk) file to public Debian binary repo based on tag and
# branch name. Designed to be run from the GitLab runner by CI.
################################################################################

set -e  # Exit on error

SSH_ARGS="-o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY root@35.215.116.197"
LOCKFILE="/debian/lockfile"
LOGFILE="/debian/deploy.log"
ATTEMPTS=10
SLEEP=20
TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
USER_NAME=$(whoami)

# Function to log messages
log_message() {
    MESSAGE=$1
    ssh $SSH_ARGS "echo '[$TIMESTAMP] $MESSAGE' >> $LOGFILE"
    # Keep only last 1000 lines of the log to prevent bloat
    ssh $SSH_ARGS "tail -n 1000 $LOGFILE > $LOGFILE.tmp && mv $LOGFILE.tmp $LOGFILE"
}

# Check if force-unlock flag is used
if [ "$1" == "--force-unlock" ]; then
    log_message "FORCING REMOVAL of stale lockfile by user $USER_NAME"
    ssh $SSH_ARGS "rm -f $LOCKFILE"
fi

# Ensure lockfile is removed on exit and log it
trap 'log_message "LOCKFILE REMOVED by $USER_NAME after deployment of $FILE on branch $CI_COMMIT_BRANCH with tag $CI_COMMIT_TAG"; ssh $SSH_ARGS "rm -f $LOCKFILE"' EXIT

echo "--> PLATFORM: $PLATFORM"
echo "--> BRANCH:   $CI_COMMIT_BRANCH"
echo "--> TAG:      $CI_COMMIT_TAG"

# Validate platform
if [[ ! "$PLATFORM" == "qrb5165" ]] && [[ ! "$PLATFORM" == "qrb5165-2" ]] && [[ ! "$PLATFORM" == "apq8096" ]]; then
    echo "WARNING: Unrecognized PLATFORM: $PLATFORM"
fi

# Ensure a .deb or .ipk file exists
NUM_PKG=$(ls -1q *.deb *.ipk 2>/dev/null | wc -l)
if [ "$NUM_PKG" -eq "0" ]; then
    echo "ERROR: Missing .deb or .ipk file"
    exit 1
elif [ "$NUM_PKG" -gt "1" ]; then
    echo "ERROR: More than 1 .deb or .ipk file found"
    exit 1
fi

# Identify package file
FILE_DEB=(*.deb)
FILE_IPK=(*.ipk)
IS_IPK=false

if [ -f "$FILE_DEB" ]; then
    FILE=$FILE_DEB
elif [ -f "$FILE_IPK" ]; then
    FILE=$FILE_IPK
    IS_IPK=true
else
    echo "ERROR: Cannot find .deb or .ipk file"
    exit 1
fi
echo "--> FILE: $FILE"

# Determine deployment section based on branch/tag
if [[ "$CI_COMMIT_BRANCH" == "dev" ]]; then
    SECTION="dev"
else
    TAG_START=$(echo $CI_COMMIT_TAG | awk '{print $1}')
    if [[ "$TAG_START" =~ ^[vV][0-9]+\.[0-9]+\.[0-9]+ ]]; then
        SECTION="staging"
    elif [[ "$TAG_START" =~ ^sdk-[0-9]+\.[0-9]+ ]]; then
        SECTION=${TAG_START%.*}
    else
        echo "WARNING: Unknown tag format. Skipping deployment."
        exit 0
    fi
fi

DEST="/debian/dists/$PLATFORM/$SECTION/binary-arm64/"
echo "--> DEST: $DEST"

# Ensure SSH key exists
if ! [ -f "$GCLOUD_REPO_VM_SSH_KEY" ]; then
    echo "ERROR: SSH key not found"
    exit 1
fi

chmod 400 "$GCLOUD_REPO_VM_SSH_KEY"

echo "Validating SSH connection..."
if ! ssh $SSH_ARGS "test -d /debian/"; then
    echo "ERROR: SSH connection failed"
    exit 1
fi

# Wait for lockfile to clear
echo "Checking for lockfile..."
ISLOCKED=1
for ((i=1; i<=ATTEMPTS; i++)); do
    if ssh $SSH_ARGS "test -e $LOCKFILE"; then
        echo "Attempt $i/$ATTEMPTS: Lockfile exists. Retrying in $SLEEP seconds..."
        sleep $SLEEP
    else
        echo "No lockfile found, proceeding with deployment."
        ISLOCKED=0
        break
    fi
done

if [ "$ISLOCKED" -eq 1 ]; then
    echo "ERROR: Lockfile still exists after $ATTEMPTS attempts. Aborting."
    exit 1
fi

# Create lockfile and log deployment details
log_message "LOCKFILE CREATED by $USER_NAME for package $FILE on branch $CI_COMMIT_BRANCH with tag $CI_COMMIT_TAG"
ssh $SSH_ARGS "touch $LOCKFILE"

# Deploy the package
echo "Deploying package..."
scp -o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY "./${FILE}" root@35.215.116.197:$DEST

# Log successful deployment
log_message "DEPLOYMENT SUCCESSFUL: $FILE pushed to $DEST by $USER_NAME"

echo "Deployment complete. Lockfile removed."
exit 0
