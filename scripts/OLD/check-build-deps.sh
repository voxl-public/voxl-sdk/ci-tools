set -e

WORK_DIR=${PWD}

echo "[INFO] - check-build-deps.sh --> running from: " $WORK_DIR

REPO=""
if [[ $# -eq 1 ]] ; then
    REPO=$1
    echo "[INFO] updating repo to pull from: "$REPO
fi


# run get_build_deps script if present
if [[ -e get_build_deps.sh ]]
then
	./get_build_deps.sh $REPO
else
	echo "[INFO] No get_build_deps script found. Moving on. "
fi

# run install_build_deps script if present
if [[ -e install_build_deps.sh ]]
then
	./install_build_deps.sh $REPO
else
	echo "[INFO] No install_build_deps script found. Moving on. "
fi
