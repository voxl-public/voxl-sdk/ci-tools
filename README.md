# ci-tools

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Summary](#summary)
- [How to Include a Shared Pipeline Configs](#how-to-include-a-shared-pipeline-configs)
- [Pipleline Configs](#pipleline-configs)
  - [.build-voxl-cross.yml](#build-voxl-crossyml)
  - [.build-voxl-emulator.yml](#build-voxl-emulatoryml)
  - [.build-voxl-hexagon.yml](#build-voxl-hexagonyml)
  - [.pkg-voxl-emulator.yml](#pkg-voxl-emulatoryml)
  - [.deploy-dev.yml](#deploy-devyml)
  - [.deploy-stable.yml](#deploy-stableyml)
  - [Nightly Builds](#nightly-builds)
- [Scripts](#scripts)
  - [deploy-opkg-repo.sh](#deploy-opkg-reposh)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Summary

Contains CI scripts and configurations to be used across different projects.

Common scripts and pipeline configurations should be maintained here.

## How to Include a Shared Pipeline Configs

The `ci-tools` repo has a `/pipeline-configs` directory.  This contains common pipeline configs that you can include into a projects `.gitlab-ci.yml` file.  You'll want to clone the repo before running the main ci script for your project to grab the scripts needed.

For example:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.build-voxl-emulator.yml'
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.build-qrb5165-emulator.yml'
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-dev.yml'
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-dev-deb.yml'
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-stable.yml'
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-stable-deb.yml'
stages:
  - build_stage
  - build_stage_qrb5165
  - deploy_stage_dev
  - deploy_stage_dev_deb
  - deploy_stage_stable
  - deploy_stage_stable_deb
```

## Pipeline Configs

### .build-voxl-cross.yml

- uses the `voxl-cross` docker image
- runs the typical `build_aarch64.sh` and `make_package.sh` scripts to generate an IPK

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.build-voxl-cross.yml'

stages:
  - build_stage
```

### .build-voxl-emulator.yml

- uses the `voxl-emulator` docker image
- runs the typical `build.sh` and `make_package.sh` scripts to generate an IPK

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.build-voxl-emulator.yml'

stages:
  - build_stage
```

### .build-qrb5165-emulator.yml

- uses the `qrb5165-emulator` docker image
- runs the typical `build.sh` and `make_package.sh` scripts to generate a Debian package

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.build-qrb5165-emulator.yml'

stages:
  - build_stage
```

### .build-voxl-hexagon.yml

- uses the `voxl-hexagon` docker image
- runs the typical `build.sh` and `make_package.sh` scripts to generate an IPK

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.build-voxl-hexagon.yml'

stages:
  - build_stage
```

### .pkg-voxl-emulator.yml

- uses the `voxl-emulator` docker image
- only runs the typical `make_package.sh` script to generate an IPK

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.pkg-voxl-emulator.yml'

stages:
  - build_stage
```

### .pkg-qrb5165-emulator.yml

- uses the `qrb5165-emulator` docker image
- only runs the typical `make_package.sh` script to generate a Debian package

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.pkg-qrb5165-emulator.yml'

stages:
  - build_stage
```

### .deploy-dev.yml

- **dev** branch must be `Protected` to access environment variables for deployment
- uses the `google/cloud-sdk` docker image
- `dev` only runs for the `dev` branch
- pushes the IPK to the ModalAI opkg `dev` repository

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-dev.yml'

stages:
  - deploy_stage_dev
```

### .deploy-dev-deb.yml

- **dev** branch must be `Protected` to access environment variables for deployment
- uses the `google/cloud-sdk` docker image
- `dev` only runs for the `dev` branch
- pushes the Debian package to the ModalAI dpkg `dev-deb` repository

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-dev-deb.yml'

stages:
  - deploy_stage_dev_deb
```


### .deploy-stable.yml

- **master** branch must be `Protected` to access environment variables for deployment
- uses the `google/cloud-sdk` docker image
- `stable` only runs when a tag is pushed following the `vM.m.b` semantic
- validates that the tag version and the IPK version match
- pushes the IPK to the ModalAI opkg `stable` repository

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-stable.yml'

stages:
  - deploy_stage_stable
```

### .deploy-stable-deb.yml

- **master** branch must be `Protected` to access environment variables for deployment
- uses the `google/cloud-sdk` docker image
- `stable` only runs when a tag is pushed following the `vM.m.b` semantic
- validates that the tag version and the Debian version match
- pushes the Debian package to the ModalAI dpkg `stable-deb` repository

How to use in your project:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.deploy-stable-deb.yml'

stages:
  - deploy_stage_stable_deb
```

### Nightly Builds

The `.nightly*` scripts will build for schedules setup in the repo's CI.

For example:

```bash
before_script:
  - git clone -q --depth 1 https://gitlab.com/voxl-public/utilities/ci-tools.git

include:
  - project: 'voxl-public/utilities/ci-tools'
    file: '/pipeline-configs/.nightly-voxl-emulator.yml'

stages:
  - nightly_build_stage
```

## Scripts

### deploy-opkg-repo.sh

Relies on `OPKG_CHANNEL` environment variable being set to either `dev` or `stable`.

For deploying, you need to set the branch (dev or master) to `Protected` in order to access gitlab ci environment variables.
