#!/bin/bash
################################################################################
# Copyright 2022 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

## THIS FILE IS DEPRECATED, use deploy-deb-repo.sh now

CONTROL_FILE=pkg/control/control

################################################################################
# Validates if the IPK version and git tag agree.  The tag must contain the
# version
# Globals:
#   CI_COMMIT_TAG
#   ORACLE_SID
# Arguments:
#   None
################################################################################
# function validate_tag_version(){
# 	ipk_version='v'$(grep "^Version: " "$CONTROL_FILE" | cut -d' ' -f2-)

# 	if [[ "$CI_COMMIT_TAG" == *"$ipk_version"* ]]; then
# 		echo "--> git tag and IPK version agree"
# 	else
# 		echo "WARNING git tag does not match IPK version in ipk/control/control"
# 		exit 1
# 	fi
# }

################################################################################
# Copies all IPKs in the current directory to the package repo
# version
# Globals:
#   GCLOUD_SERVICE_KEY
#   GCLOUD_COMPUTE_INSTANCE
#   GCLOUD_PROJECT
# Arguments:
#   channel - should be 'stable' or 'dev'
################################################################################
function deploy_to_gcp(){
	CHANNEL=$1
	gcloud auth activate-service-account --key-file=$GCLOUD_SERVICE_KEY


	IPK_NAME=(*.ipk)
	DEB_NAME=(*.deb) ## TODO remove this once deb migration is done

	echo "IPK_NAME: $IPK_NAME"
	echo "DEB_NAME: $DEB_NAME"

	## for dev branch, push ipk
	if [ "$CHANNEL" == "dev" ]; then

		## check a package was built
		if [ ! -f "$IPK_NAME" ]; then
			echo "[ERROR] ipk not found: $IPK_NAME"
			exit 1
		fi

		VERSION=$(cat "$CONTROL_FILE" | grep "Version" | cut -d' ' -f 2)
		PACKAGE=$(cat "$CONTROL_FILE" | grep "Package" | cut -d' ' -f 2)

		## make new name with timestamp (only for ipk)
		dts=$(date +"%Y%m%d%H%M")
		newname=${PACKAGE}_${VERSION}'_'$dts'.ipk'
		echo "new ipk name with timestamp: $newname"

		## rename if the package doesn't already have the timestamp
		if ! [[ "$IPK_NAME" == "$newname" ]]; then
			mv $IPK_NAME $newname
		fi

		echo "--> deploying $newname to: $GCLOUD_COMPUTE_INSTANCE:/opkg/"$CHANNEL
		gcloud --project $GCLOUD_PROJECT compute scp $"$newname" $GCLOUD_COMPUTE_INSTANCE:/opkg/$CHANNEL --zone us-west2-a


	## for dev-deb branch, push a deb
	## Deprecated. Shouldn't be used anymore.
	elif [ "$CHANNEL" == "dev-deb" ]; then

		## check a package was built
		if [ ! -f "$DEB_NAME" ]; then
			echo "[ERROR] no deb package found"
			exit 1
		fi

		## push deb package, no need to do tiemstamp here
		## the make package script should do this now for debs
		echo "--> deploying $DEB_NAME to: $GCLOUD_COMPUTE_INSTANCE:/opkg/"$CHANNEL
		gcloud --project $GCLOUD_PROJECT compute scp $"$DEB_NAME" $GCLOUD_COMPUTE_INSTANCE:/opkg/$CHANNEL --zone us-west2-a


	## stable ipk repo for now, TODO add VOXL_SDK deb versions
	elif [ "$CHANNEL" == "stable" ]; then

		## check a package was built
		if [ ! -f "$IPK_NAME" ]; then
			echo "[ERROR] ipk not found: $IPK_NAME"
			exit 1
		fi
		echo "--> deploying $IPK_NAME to: $GCLOUD_COMPUTE_INSTANCE:/opkg/"$CHANNEL
		gcloud --project $GCLOUD_PROJECT compute scp $"$IPK_NAME" $GCLOUD_COMPUTE_INSTANCE:/opkg/$CHANNEL --zone us-west2-a

	else
		echo "ERROR repo ${CHANNEL} not yet supported in deploy-opkg-depo.sh in ci-tools"
		exit 1

	fi

	# prevent gcp instance SSH key limit from getting hit
	for i in $(gcloud compute os-login ssh-keys list);
	do
		if [ "$i" != "FINGERPRINT" ] && [ "$i" != "EXPIRY" ]; then
			echo "--> removing key:"
			gcloud compute os-login ssh-keys remove --key $i;
		fi
	done
}


# exit on error
set -e

WORK_DIR=${PWD}

echo "--> running from: " $WORK_DIR
echo "--> using channel: " $OPKG_CHANNEL

# control file should be in the new location: pkg/control/control
# or the old location ipk/control/control, decide which one
if [ ! -f "$CONTROL_FILE" ]; then
	CONTROL_FILE="ipk/control/control"
	if [ ! -f "$CONTROL_FILE" ]; then

		## location specific to voxl-suite
		CONTROL_FILE="apq8096/control/control"
		if [ ! -f "$CONTROL_FILE" ]; then
			echo "failed to find control file"
			exit 1
		fi
	fi
fi



# prevent no ipks generating a blank .ipk found
shopt -s nullglob
if [[ -n $(echo *.ipk) ]]
then
	if [[ "$OPKG_CHANNEL" == "stable" ]]; then

			# grab first word
			TAG_START=$(echo $CI_COMMIT_TAG | awk '{print $1}')
			# versioned tags go to staging
		if ! [[ $TAG_START =~ ^[vV][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}(.*)$ ]]; then
			echo "WARNING tag should specify version vx.y.z or sdk-x.y"
			echo "skipping deploying this package for unknown tag format"
			exit 0
		fi
	fi
fi

deploy_to_gcp $OPKG_CHANNEL
