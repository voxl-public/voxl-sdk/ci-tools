#!/bin/bash
################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# deploys a px4 image for flight core to the public binary repo based on branch
# name.
################################################################################


SSH_ARGS="-o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY root@35.215.116.197"

# exit on error
set -e

echo "--> PLATFORM: " $PLATFORM
echo "--> BRANCH:   " $CI_COMMIT_BRANCH
echo "--> TAG:      " $CI_COMMIT_TAG

# check platform is currently supported and warn if otherwise
if [[ ! "$PLATFORM" ==  "fc-v2" ]] && [[ ! "$PLATFORM" ==  "fc-v1" ]]; then
	echo "WARNING, unrecognized PLATFORM: $PLATFORM"
	echo "should be fc-v2 or fcv1"
	echo "continuing anyway, but with undefined behavior"
fi

ls
ls px4-firmware/build/modalai_${PLATFORM}_default/modalai_${PLATFORM}_*.px4

# check that target was actually built
NUM_IMAGES=$(ls -1q px4-firmware/build/modalai_${PLATFORM}_default/modalai_${PLATFORM}_*.px4 2>/dev/null | wc -l)

if [ $NUM_IMAGES -eq "0" ]; then
	echo "ERROR: missing px4 build image"
	exit 1
elif [ $NUM_IMAGES -gt "1" ]; then
	echo "ERROR: more than 1 px4 build image found"
	exit 1
fi


FILE=(px4-firmware/build/modalai_${PLATFORM}_default/modalai_${PLATFORM}_*.px4)
echo "--> FILE:      $FILE"
BIN_FILE=(px4-firmware/build/modalai_${PLATFORM}_default/modalai_${PLATFORM}_*.bin)
echo "--> BIN_FILE:  $BIN_FILE"





# set destination of the file based on branch and tag
if [[ "$CI_COMMIT_BRANCH" == "dev" ]]; then

	SECTION="dev"

# for branches other than dev (master and maintenance branches) figure out
# what to do based on tag. version tags go to staging
# sdk tags go to sdk repos.
else
	WORDS=$(echo $CI_COMMIT_TAG | wc -w)
	if [ $WORDS -eq 0 ]; then
		echo "ERROR empty commit tag"
		exit 1
	fi

	# grab first word
	TAG_START=$(echo $CI_COMMIT_TAG | awk '{print $1}')

	# versioned tags go to staging
	if [[ "$TAG_START" =~ ^[vV][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$ ]] ||
	[[  "$TAG_START" =~ ^[vV][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}-[0-9]{1,3}$ ]] ||
	[[  "$TAG_START" =~ ^[vV][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}-[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$ ]] ||
	[[  "$TAG_START" =~ ^[0-9]{1,3}:[vV][0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}-[0-9]{1,3}$ ]]; then
		SECTION="staging"
	# sdk tags go to an sdk folder
	elif [[ $TAG_START =~ ^sdk-[0-9]{1,3}.[0-9]{1,3}$ ]]; then
		SECTION=$TAG_START
	elif [[ $TAG_START =~ ^sdk-[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3} ]]; then
		SECTION=${TAG_START%.*} ## trim off last patch number
	else
		echo "WARNING tag should specify version vx.y.z sdk-x.y or sdk-x.y.z"
		echo "skipping deploying this package for unknown tag format"
		exit 0
	fi
fi











## construct the final path on google VM to push to
DEST="/debian/dists/${PLATFORM}/${SECTION}"
echo "--> DEST:      $DEST"


## pull the version number out of the control file
if [ ! -f "pkg/control/control" ]; then
	echo "ERROR failed to find pkg/control/control"
	echo "make sure it's in the CI artifacts"
	exit 1
fi
VERSION=$(cat pkg/control/control | grep "Version" | cut -d' ' -f 2)

## on dev builds only add a timestamp
if [[ $TARGET_DIR == "dev" ]]; then
	dts=$(date +"%Y%m%d%H%M")
	VERSION="${VERSION}_${dts}"
fi

## construct the new image name to upload
NEW_IMAGE_NAME=modalai_${PLATFORM}_${VERSION}.px4
echo "New image name: $NEW_IMAGE_NAME"
NEW_BIN_IMAGE_NAME=modalai_${PLATFORM}_${VERSION}.bin
echo "New image name: $NEW_BIN_IMAGE_NAME"


if ! [ -f $GCLOUD_REPO_VM_SSH_KEY ]; then
	echo "ERROR failed to find ssh key"
	echo "Make sure tag wildcards 'v*' & 'sdk*' are protected"
	echo "as well as dev and master branches"
	exit 1
fi

echo "setting permissions for key"
chmod 400 $GCLOUD_REPO_VM_SSH_KEY

echo "validating ssh is working"
if ssh $SSH_ARGS "test -e /debian/"; then
	echo "SSH worked"
else
	echo "ssh failed"
	exit 1
fi

# checking for lockfile
ISLOCKED=true
ATTEMPTS="10"
SLEEP="20"
for ((i=1; i <= $ATTEMPTS; i++)); do
	if ssh $SSH_ARGS "test -e /debian/lockfile"; then
		echo "deploy attempt $i of $ATTEMPTS: deb repo is locked. sleeping for $SLEEP seconds."
		sleep $SLEEP
	else
		echo "no lockfile found, deploying package"
		ISLOCKED=false
		break
	fi
done

if [ $ISLOCKED == true ]; then
	echo "deb repo still locked after $ATTEMPTS attempts, giving up"
	exit 1
fi

# push to the repo
echo "pushing using scp"
scp -o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY "$FILE" root@35.215.116.197:$DEST/$NEW_IMAGE_NAME
scp -o StrictHostKeyChecking=no -i $GCLOUD_REPO_VM_SSH_KEY "$BIN_FILE" root@35.215.116.197:$DEST/$NEW_BIN_IMAGE_NAME

echo "DONE"
exit 0
